package characters.basic;

import java.awt.Image;


public interface Character {
	Image walk(String name, int frequency);
}
