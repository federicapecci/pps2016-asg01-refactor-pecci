package characters.basic;

import objects.GameObject;
import objects.Piece;

import java.awt.*;

/**
 * Created by Federica on 17/03/17.
 */
public interface Protagonist {

    public boolean isJumping();

    public void setJumping(boolean jumping);

    public Image doJump();

    public void contact(GameObject obj);

    public boolean contactPiece(Piece piece);

    public void contact(BasicCharacter pers);


}
