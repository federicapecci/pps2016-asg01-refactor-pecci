package characters.basic;

import game.Main;
import objects.GameObject;
import objects.Piece;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Federica on 18/03/17.
 */
public class ProtagonistImpl extends BasicCharacter implements Protagonist {


    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private boolean jumping;
    private int jumpingExtent;

    public ProtagonistImpl(int x, int y, int WIDTH, int HEIGHT) {
        super(x, y, WIDTH, HEIGHT);

    }

    @Override
    public boolean isJumping() {
            return jumping;
    }

    @Override
    public void setJumping(boolean jumping) {
            this.jumping = jumping;
    }

    @Override
    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    @Override
    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            Main.scene.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            Main.scene.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                Main.scene.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                Main.scene.setHeightLimit(0); // initial sky
            }
        }
    }

    @Override
    public boolean contactPiece(Piece piece) {
        return this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece);
    }

    @Override
    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) || this.hitBack(pers)) {
            if (pers.alive) {
                this.setMoving(false);
                this.setAlive(false);
            } else this.alive = true;
        } else if (this.hitBelow(pers)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
    }
}
