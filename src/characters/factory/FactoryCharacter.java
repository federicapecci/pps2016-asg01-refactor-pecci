package characters.factory;

import characters.basic.Protagonist;
import characters.minor.Antagonist;

/**
 * Created by Federica on 18/03/17.
 */
public interface FactoryCharacter {

    public Protagonist createMario(int x, int y);

    public Antagonist createMushroom(int x, int y);

    public Antagonist createTurtle(int x, int y);


}
