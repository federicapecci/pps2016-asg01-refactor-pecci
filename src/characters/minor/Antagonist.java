package characters.minor;

import characters.basic.BasicCharacter;
import objects.GameObject;

import java.awt.*;

/**
 * Created by Federica on 17/03/17.
 */
public interface Antagonist {

    public void move();

    public void run();

    public void contact(GameObject obj);

    public void contact(BasicCharacter pers);

    public abstract Image deadImage();



}
