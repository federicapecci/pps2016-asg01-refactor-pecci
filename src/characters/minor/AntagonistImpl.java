package characters.minor;

import characters.basic.BasicCharacter;
import objects.GameObject;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Federica on 17/03/17.
 */
public class AntagonistImpl extends BasicCharacter implements Antagonist, Runnable{

    private final int PAUSE = 15;
    private int offsetX;

    public AntagonistImpl(int y, int x, int WIDTH, int HEIGHT){
        super(y, x, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;

        Thread chronoAntagonist = new Thread(this);
        chronoAntagonist.start();
    }

    @Override
    public void move() {

        this.offsetX = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Override
    public void contact(GameObject obj) {

        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    @Override
    public void contact(BasicCharacter pers) {

        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }

    }


    public Image deadImage(){
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
