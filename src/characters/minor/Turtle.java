package characters.minor;

import java.awt.Image;

import characters.minor.Antagonist;
import characters.minor.AntagonistImpl;
import utils.Res;
import utils.Utils;

public class Turtle extends AntagonistImpl implements Antagonist {

    private  static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private Image imgTurtle;


    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        this.imgTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);
    }

    public Image getImg() {
        return imgTurtle;
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
