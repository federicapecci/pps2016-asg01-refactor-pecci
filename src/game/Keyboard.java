package game;


import utils.Utils;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.TimerTask;


public class Keyboard implements KeyListener {


    /*istanzio mario, mushroom e turtle con factory method

    FactoryCharacter factoryCharacter = new FactoryCharacterImpl();

    Protagonist mario =  factoryCharacter.createMario(300, 245);
    Antagonist mushroom = factoryCharacter.createMushroom(800, 263);
    Antagonist turtle = factoryCharacter.createTurtle(950, 243);*/


    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.mario.isAlive()) {

            switch (e.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                    // per non fare muovere il castello e start
                    if (Main.scene.getxPos() == -1) {
                        Main.scene.setxPos(0);
                        Main.scene.setBackground1PosX(-50);
                        Main.scene.setBackground2PosX(750);
                    }
                    Main.scene.mario.setMoving(true);
                    Main.scene.mario.setToRight(true);
                    Main.scene.setMov(1); // si muove verso sinistra
                    break;

                case KeyEvent.VK_LEFT:
                    if (Main.scene.getxPos() == 4601) {
                        Main.scene.setxPos(4600);
                        Main.scene.setBackground1PosX(-50);
                        Main.scene.setBackground2PosX(750);
                    }

                    Main.scene.mario.setMoving(true);
                    Main.scene.mario.setToRight(false);
                    Main.scene.setMov(-1); // si muove verso destra
                    break;

                case KeyEvent.VK_UP:
                    Main.scene.mario.setJumping(true);
                    Utils.playSound("/resources/audio/jump.wav");
                    break;

                default:
                    break;
            }

        }else{

            Utils.playSound("/resources/audio/fail-trombone-01.wav");

            try
            {
                Thread.sleep(4000);
            }
            catch (InterruptedException interruptedException)
            {
                interruptedException.printStackTrace();
            }

            System.exit(0);


        }


    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.mario.setMoving(false);
        Main.scene.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
