package game;

import characters.basic.Mario;
import characters.minor.Mushroom;
import characters.minor.Turtle;
import objects.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    public Mario mario;
    public Mushroom mushroom;
    public Turtle turtle;

    private Image imgFlag;
    private Image imgCastle;



    private ArrayList<GameObject> objects;
    private ArrayList<Piece> pieces;


    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        mario = new Mario(300, 245);
        mushroom = new Mushroom(800, 263);
        turtle = new Turtle(950, 243);



        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        objects = new ArrayList<GameObject>();


        this.objects.add(new Tunnel(600, 230));
        this.objects.add(new Tunnel(1000, 230));
        this.objects.add(new Tunnel(1600, 230));
        this.objects.add(new Tunnel(1900, 230));
        this.objects.add(new Tunnel(2500, 230));
        this.objects.add(new Tunnel(3000, 230));
        this.objects.add(new Tunnel(3800, 230));
        this.objects.add(new Tunnel(4500, 230));

        this.objects.add(new Block(400, 180));
        this.objects.add(new Block(1200, 180));
        this.objects.add(new Block(1270, 170));
        this.objects.add(new Block(1340, 160));
        this.objects.add(new Block(2000, 180));
        this.objects.add(new Block(2600, 160));
        this.objects.add(new Block(2650, 180));
        this.objects.add(new Block(3500, 160));
        this.objects.add(new Block(3550, 140));
        this.objects.add(new Block(4000, 170));
        this.objects.add(new Block(4200, 200));
        this.objects.add(new Block(4300, 210));

        pieces = new ArrayList<Piece>();

        this.pieces.add(new Piece(402, 145));
        this.pieces.add(new Piece(1202, 140));
        this.pieces.add(new Piece(1272, 95));
        this.pieces.add(new Piece(1342, 40));
        this.pieces.add(new Piece(1650, 145));
        this.pieces.add(new Piece(2650, 145));
        this.pieces.add(new Piece(3000, 135));
        this.pieces.add(new Piece(3400, 125));
        this.pieces.add(new Piece(4200, 145));
        this.pieces.add(new Piece(4600, 40));

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;
        ArrayList<Piece> piecesToDelete = new ArrayList<>();

        this.objects.forEach(i -> {
            if (this.mario.isNearby(i))
                this.mario.contact(i);

            if (this.mushroom.isNearby(i))
                this.mushroom.contact(i);

            if (this.turtle.isNearby(i))
                this.turtle.contact(i);
        });

        this.pieces.forEach(i -> {

            if (this.mario.contactPiece(i)) {
                Utils.playSound(Res.AUDIO_MONEY);
                piecesToDelete.add(i);

            }
        });

        //piecesToDelete list is necessary to avoid concurrent modifications in pieces list
        piecesToDelete.forEach(i -> {
            this.pieces.remove(i);
        });
        piecesToDelete.clear();

        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.objects.forEach( i -> {
                i.move();
            });

            this.pieces.forEach(i -> {
                i.move();
            });

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        this.objects.forEach(i -> {
            g2.drawImage(i.getImgObj(), i.getX(),
                    i.getY(), null);
        });

        this.pieces.forEach(i -> {
            g2.drawImage(i.imageOnMovement(), i.getX(),
                    i.getY(), null);
        });

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g2.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);

        if (this.mushroom.isAlive())
            g2.drawImage(this.mushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.mushroom.getX(), this.mushroom.getY(), null);
        else
            g2.drawImage(this.mushroom.deadImage(), this.mushroom.getX(), this.mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.turtle.isAlive())
            g2.drawImage(this.turtle.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.turtle.getX(), this.turtle.getY(), null);
        else
            g2.drawImage(this.turtle.deadImage(), this.turtle.getX(), this.turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
    }


}
