package utils;


import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Roberto Casadei
 */

public class Utils {

    public static URL getResource(String path){
        return Utils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }

    public static void playSound(String son) {
        Clip clip;
        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(Utils.class.getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
            clip.start();
        } catch (Exception e) {
            // TODO: log error
        }


    }

}
